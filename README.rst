Codebeispiele zum Impulsvortrag vom 23.09.2020
==============================================

Diese Beispiele sind Demos und wenn ich etwas getan habe, was man nur in Demos tun darf und nicht im echten Leben, dann habe ich das kommentiert! :-)

Beispiele:

* Textverarbeitung, darf eine Mail dunkel verarbeitet werden? (zu Demozwecken wurde ein winzig kleiner Trainingsdatensatz aus Phantasienachrichten benutzt, da es nur ums Prinzip geht :-))
* Bildverarbeitung, gleiche Texturen in Bildern in der gleichen Farbe einfärben (funktioniert zu Demozwecken nur auf großen Bildausschnitten und nicht für winzige Bereiche, um alle Kanten sauber zu erwischen)
* Zeitreihenprognose, Prognose von Coronainfizierten in Deutschland (auch hier wurde zu Demozwecken geschummelt und ignoriert, daß es viele Einflußfaktoren auf den Anstieg der Infektionsrate gibt, wie z.B. das Wetter, Schulferien...)
* Signalverarbeitung (hier fehlt mir noch eine Demo und ich hoffe, ich kann noch eine fertig stellen)


Für jedes Beispiel existiert ein Ordner, der alle nötigen Dateien und ein Jupyter Notebook enthält. Das Jupyter Notebook enthält jeweils den ausführbaren Pythoncode. Dazu muß auf dem Rechner am besten Python 3.8 installiert sein.

Die nötigen Python Packages sind in requirements.txt zu finden. Das ganze sollte in einer Virtualenv mit Python 3.7 oder 3.8 laufen. Ich habe für jedes Beispiel eine eigene Virtualenv, aber eine gemeinsame funktioniert auch.
Mit dem Befehl sudo snap install cmake --classic zunächst cmake installieren und dann mittels pip install -r requirements.txt die Python packages installieren.

Starte das Jupyter Notebook mit: jupyter notebook, es öffnet sich im Browser. Nun bitte die gewünschte *.ipynb Datei anklicken

Commands:

.. code-block:: bash

    python3 -m venv env
    source  env/bin/activate
    sudo snap install cmake --classic
    pip install -r requirements.txt
    jupyter notebook
